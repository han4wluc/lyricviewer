//
//  WordMeaningCollectionViewCell.h
//  LyricViewer
//
//  Created by Luciano Wu on 17/03/15.
//  Copyright (c) 2015 han4wluc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WordMeaningCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *wordLabel;
@property (strong, nonatomic) IBOutlet UILabel *meaningLabel;

@end
