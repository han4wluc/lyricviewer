//
//  LyricLineCollectionViewCell.h
//  LyricViewer
//
//  Created by Luciano Wu on 17/03/15.
//  Copyright (c) 2015 han4wluc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LyricLineCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *lineLabel;

@end
